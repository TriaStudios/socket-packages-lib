﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    /// <summary>
    ///     Generic interface for handler of project's packages
    /// </summary>
    /// <typeparam name="T">Package type</typeparam>
    /// <typeparam name="TSession">Entity data type</typeparam>
    public abstract class Handler<T, TSession> : IHandler
        where TSession : NetworkSession, new()
    {
        private INetworkSocket<TSession> _networkSocket;

        public void RegisterHandler(object networkSocket) => RegisterHandler((INetworkSocket<TSession>) networkSocket);
        private void RegisterHandler(INetworkSocket<TSession> networkSocket) => _networkSocket = networkSocket;

        public Type ModelType => typeof(T);
        protected IEnumerable<TSession> ConnectedSessions => _networkSocket.Sessions;

        protected virtual Task HandleAsync(TSession session, T model) { return Task.CompletedTask; }
        protected virtual void Handle(TSession session, T model) { }

        public Task HandleAsync(object session, object model)
        {
            Handle((TSession) session, (T) model);
            return HandleAsync((TSession) session, (T) model);
        }
    }
}
