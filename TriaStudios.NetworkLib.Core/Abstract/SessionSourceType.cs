using System;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    public enum SessionSourceType
    {
        Client,
        Server
    }

    public static class SessionSourceTypeExtensions
    {
        public static SessionSourceType Reverse(this SessionSourceType sourceType)
        {
            switch (sourceType)
            {
                case SessionSourceType.Client: return SessionSourceType.Server;
                case SessionSourceType.Server: return SessionSourceType.Client;
                default: throw new ArgumentNullException(nameof(sourceType));
            }
        }
    }
}
