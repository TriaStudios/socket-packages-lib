﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    /// <summary>
    /// Abstract class for custom session data
    /// </summary>
    public abstract class NetworkSession
    {
        internal SocketAsyncEventArgs WriteEventArg { get; set; }
        internal ISocketSender<NetworkSession> Socket { get; set; }
        internal IPackagesService PackagesService { get; set; }
        internal ILogger Logger { get; set; }
        internal IMiddleware Pipeline { get; set; }

        public NetworkConnectionInfo FromInfo { get; internal set; }
        public NetworkConnectionInfo ToInfo { get; internal set; }
        

        private readonly object lockObj = new object();

        /// <summary>
        /// Converting package to byte array and send it. Can be called asynchronized.
        /// </summary>
        /// <typeparam name="T">Type of package</typeparam>
        /// <param name="model">package</param>
        public void Send<T>(T model)
        {
            try
            {
                lock (lockObj)
                {
                    var package = PackagesService.GetBytes(model);
                    package = Pipeline.Forward(this, package);
                    var data = new byte[package.Length + 4];
                    package.CopyTo(data, 4);
                    BitConverter.GetBytes(package.Length).CopyTo(data, 0);
                    WriteEventArg.SetBuffer(data, 0, data.Length);
                    Socket.Send(WriteEventArg);
                }
            }
            catch (Exception e)
            {
                Logger.Debug(LoggerLayer.Error, $"{e.Message} {{{e.StackTrace}}}");
            }
        }

        /// <summary>
        /// Converting byte array sent by connection to package and call package's handler
        /// </summary>
        /// <param name="data">package</param>
        /// <param name="ioCService">IoC</param>
        internal async Task OnReceivedAsync(byte[] data, IIoCService ioCService)
        {
            data = Pipeline.Backward(this, data);
            var model = ioCService.GetInstance<IPackagesService>().GetModel(data);
            await ioCService
                .GetInstance<IHandlerService>()
                .GetHandler(model.GetType(), Socket)
                .HandleAsync(this, model);
        }
    }
}
