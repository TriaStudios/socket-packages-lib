using System.Net;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    public class NetworkConnectionInfo
    {
        public EndPoint EndPoint { get; internal set; }
        public SessionSourceType SourceType { get; set; }
    }
}
