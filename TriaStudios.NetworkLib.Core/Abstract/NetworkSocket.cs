﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Models;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    /// <summary>
    /// Socket for network connection
    /// </summary>
    /// <typeparam name="T">Custom session type</typeparam>
    public abstract class NetworkSocket<T> : ISocketSender<T>, IDisposable where T : NetworkSession, new()
    {
        public IIoCService IoCService { get; }

        protected IPEndPoint EndPoint { get; set; }
        protected Socket Socket { get; set; }
        protected abstract SessionSourceType SourceType { get; }
        private readonly IMiddleware _pipeline;
        private readonly ILogger _logger;

        private readonly ConcurrentDictionary<NetworkSession, Memory<byte>> _receiveStream =
            new ConcurrentDictionary<NetworkSession, Memory<byte>>();

        private readonly ConcurrentDictionary<NetworkSession, Memory<byte>> _receivePackages =
            new ConcurrentDictionary<NetworkSession, Memory<byte>>();

        protected NetworkSocket(IPAddress address, int port, IIoCService ioCService, IMiddleware pipeline)
        {
            IoCService = ioCService;
            EndPoint = new IPEndPoint(address, port);
            _pipeline = pipeline;
            _logger = IoCService.GetInstance<ILogger>();
        }

        protected void ProcessConnect(SocketAsyncEventArgs e)
        {
            // create read socket connection
            var readEventArgs = new SocketAsyncEventArgs();
            readEventArgs.Completed += IO_Completed;
            readEventArgs.AcceptSocket = e.AcceptSocket;

            // create write socket connection
            var writeEventArgs = new SocketAsyncEventArgs();
            writeEventArgs.Completed += IO_Completed;
            writeEventArgs.AcceptSocket = e.AcceptSocket;

            var state = new StateObject(readEventArgs, writeEventArgs)
            {
                Session = new T
                {
                    Socket = this,
                    WriteEventArg = writeEventArgs,
                    PackagesService = IoCService.GetInstance<IPackagesService>(),
                    FromInfo = new NetworkConnectionInfo
                    {
                        EndPoint = readEventArgs.AcceptSocket.LocalEndPoint,
                        SourceType = SourceType
                    },
                    ToInfo = new NetworkConnectionInfo
                    {
                        EndPoint = readEventArgs.AcceptSocket.RemoteEndPoint,
                        SourceType = SourceType.Reverse()
                    },
                    Pipeline = _pipeline,
                    Logger = _logger
                }
            };
            _receiveStream.AddOrUpdate(state.Session, Memory<byte>.Empty, (session, memory) => memory);
            _receivePackages.AddOrUpdate(state.Session, Memory<byte>.Empty, (session, block) => block);
            OnConnectInternal((T)state.Session);

            // wait received message
            var willRaiseEvent = e.AcceptSocket.ReceiveAsync(readEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(readEventArgs);
            }
        }


        #region Messages handlers

        private void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            // determine which type of operation just completed and call the associated handler
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;
                case SocketAsyncOperation.Accept:
                case SocketAsyncOperation.Connect:
                case SocketAsyncOperation.Disconnect:
                case SocketAsyncOperation.None:
                case SocketAsyncOperation.ReceiveFrom:
                case SocketAsyncOperation.ReceiveMessageFrom:
                case SocketAsyncOperation.SendPackets:
                case SocketAsyncOperation.SendTo:
                default:
                    throw new ArgumentException("The last operation completed on the socket was not a receive or send");
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            while (true)
            {
                var state = (StateObject) e.UserToken;
                state.ReceivedBytes = e.BytesTransferred;

                if (state.ReceivedBytes > 0 && e.SocketError == SocketError.Success)
                {
                    using (var ms = new MemoryStream())
                    {
                        ms.Write(_receiveStream[state.Session].ToArray(), 0, _receiveStream[state.Session].Length);
                        ms.Write(state.ReadBuffer, state.ReceivedOffset, state.ReceivedBytes - state.ReceivedOffset);
                        _receiveStream[state.Session] = ms.ToArray();
                    }

                    if (_receiveStream[state.Session].Length < 4)
                    {
                        state.ReceivedOffset = 0;
                        return;
                    }
                    var packageSize = BitConverter.ToInt32(_receiveStream[state.Session].Slice(0, 4).ToArray(), 0);
                    if (_receiveStream[state.Session].Length - 4 < packageSize)
                    {
                        state.ReceivedOffset = 0;
                        return;
                    }
            
                    state.Session.OnReceivedAsync(_receiveStream[state.Session].Slice(4, packageSize).ToArray(), IoCService).GetAwaiter().GetResult();
                    _receiveStream[state.Session] = _receiveStream[state.Session].Slice(4 + packageSize);

                    if (!_receiveStream[state.Session].IsEmpty)
                    {
                        state.ReceivedOffset = state.ReceivedBytes;
                        continue;
                    }
                    state.ReceivedOffset = 0;

                    // start recursion receiving messages
                    var willRaiseEvent = e.AcceptSocket.ReceiveAsync(e);
                    if (!willRaiseEvent)
                    {
                        continue;
                    }
                }
                else
                {
                    CloseClientSocket(e);
                }

                break;
            }
        }

        private void ProcessSend(SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
            {
                CloseClientSocket(e);
            }
        }

        public void Send(SocketAsyncEventArgs e)
        {
            e.AcceptSocket.Send(e.Buffer);
            ProcessSend(e);
        }

        #endregion

        protected void DisposeDictionaries(T session)
        {
            _receiveStream.TryRemove(session, out _);
            _receivePackages.TryRemove(session, out _);
        }

        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            _logger.Debug(LoggerLayer.Debug, "Close client socket");
            var state = e.UserToken as StateObject;

            try { e.AcceptSocket.Shutdown(SocketShutdown.Both); }
            catch (Exception)
            {
                // ignored
            }
            
            e.AcceptSocket.Close();
            state?.ReadEventArg.Dispose();
            state?.WriteEventArg.Dispose();
            OnDisconnectInternal((T)state?.Session);
        }

        private void CloseConnection()
        {
            try
            {
                Socket.Shutdown(SocketShutdown.Both);
                Socket.Close();
            }
            catch
            {
                _logger.Debug(LoggerLayer.Error, "Close connection error");
            }
        }

        public void Dispose() => CloseConnection();

        ~NetworkSocket() => Dispose();

        protected abstract void CreateConnection();
        protected abstract void OnConnectInternal(T session);
        protected abstract void OnDisconnectInternal(T session);
        public abstract IReadOnlyCollection<T> Sessions { get; }

        public static NetworkSocketBuilder<T> Builder() => new NetworkSocketBuilder<T>();
        
        public async Task RunAsync(CancellationToken cancellationToken = default)
        {
            CreateConnection();
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(1, cancellationToken);
            }
        }
    }
}
