﻿namespace TriaStudios.NetworkLib.Core.Abstract
{
    /// <summary>
    /// Class for IoC's callbacks
    /// </summary>
    public abstract class IoCBehaviour
    {
        /// <summary>
        ///     Called after service initialize
        /// </summary>
        public virtual void Start()
        {

        }
    }
}
