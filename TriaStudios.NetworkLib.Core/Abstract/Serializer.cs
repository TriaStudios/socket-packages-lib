﻿using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Abstract
{
    public abstract class Serializer<T> : ISerializer
    {
        public abstract byte[] Serialize(T model, IResolver resolver);
        public abstract T Deserialize(byte[] data, IResolver resolver);

        byte[] ISerializer.Serialize(object model, IResolver resolver) => Serialize((T)model, resolver);
        object ISerializer.Deserialize(byte[] data, IResolver resolver) => Deserialize(data, resolver);
    }
}
