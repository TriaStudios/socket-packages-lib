﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core
{
    /// <summary>
    /// Server for network connections
    /// </summary>
    /// <typeparam name="T">Custom session type</typeparam>
    internal class NetworkServer<T> : NetworkSocket<T> where T : NetworkSession, new()
    {
        private HashSet<T> Clients { get; set; }

        public override IReadOnlyCollection<T> Sessions => Clients;

        internal NetworkServer(IPAddress address, int port, IIoCService ioCService, IMiddleware middleware) 
            : base(address, port, ioCService, middleware)
        {
            Clients = new HashSet<T>();
        }

        protected override SessionSourceType SourceType => SessionSourceType.Server;

        protected override void CreateConnection()
        {
            Socket = new Socket(EndPoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            Socket.Bind(EndPoint);
            Socket.Listen(100);

            StartAccept(null);
        }

        private void StartAccept(SocketAsyncEventArgs acceptEventArg)
        {
            while (true)
            {
                // create new accept connection session data if none
                if (acceptEventArg == null)
                {
                    acceptEventArg = new SocketAsyncEventArgs();
                    acceptEventArg.Completed += AcceptEventArg_Completed;
                }
                else
                {
                    acceptEventArg.AcceptSocket = null;
                }

                // waiting new connection to server
                var willRaiseEvent = Socket.AcceptAsync(acceptEventArg);
                if (willRaiseEvent) return;
                ProcessConnect(acceptEventArg);
            }
        }

        private void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessConnect(e);
            StartAccept(e);
        }

        #region Sessions handlers

        protected override void OnConnectInternal(T session)
        {
            Clients.Add(session);
            OnConnected?.Invoke(session);
        }

        protected override void OnDisconnectInternal(T session)
        {
            DisposeDictionaries(session);
            Clients.Remove(session);
            OnDisconnected?.Invoke(session);
        }

        #endregion

        #region Handlers

        internal event Action<T> OnConnected;
        internal event Action<T> OnDisconnected;

        #endregion
    }
}
