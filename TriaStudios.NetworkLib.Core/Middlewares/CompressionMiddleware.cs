﻿using System.IO;
using System.IO.Compression;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    public class CompressionMiddleware : IMiddleware
    {
        private readonly CompressionLevel _level;
        
        public CompressionMiddleware(CompressionLevel level)
        {
            _level = level;
        }
        
        public byte[] Forward(NetworkSession session, byte[] data)
        {
            using (var output = new MemoryStream())
            using (var zip = new GZipStream(output, _level, true))
            using (var input = new MemoryStream(data))
            {
                input.CopyTo(zip);
                zip.Close();

                return output.ToArray();
            }
        }

        public byte[] Backward(NetworkSession session, byte[] data)
        {
            using (var ms = new MemoryStream(data))
            using (var zip = new GZipStream(ms, CompressionMode.Decompress))
            using (var output = new MemoryStream())
            {
                zip.CopyTo(output);
                zip.Close();

                return output.ToArray();
            }
        }
    }
}
