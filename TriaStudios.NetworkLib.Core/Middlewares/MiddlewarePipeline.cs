﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    internal class MiddlewarePipeline : IMiddleware
    {
        private readonly IEnumerable<IMiddleware> _middlewares;

        public MiddlewarePipeline(IEnumerable<Func<IIoCService, IMiddleware>> middlewares, IIoCService ioCService)
        {
            _middlewares = middlewares.Select(x => x(ioCService));
        }

        public byte[] Forward(NetworkSession session, byte[] data) =>
            _middlewares.Aggregate(data, (current, middleware) => middleware.Forward(session, current));

        public byte[] Backward(NetworkSession session, byte[] data) =>
            _middlewares.Reverse()
                .Aggregate(data, (current, middleware) => middleware.Backward(session, current));
    }
}
