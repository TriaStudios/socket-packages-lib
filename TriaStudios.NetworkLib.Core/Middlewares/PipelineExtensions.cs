﻿using System.IO.Compression;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    public static class PipelineExtensions
    {
        public static IPipelineBuilder UseCompression(this IPipelineBuilder pipelineBuilder, CompressionLevel level) =>
            pipelineBuilder.Use(new CompressionMiddleware(level));

        public static IPipelineBuilder UseEncryption(this IPipelineBuilder pipelineBuilder) =>
            pipelineBuilder.Use<EncryptionMiddleware>();
    }
}
