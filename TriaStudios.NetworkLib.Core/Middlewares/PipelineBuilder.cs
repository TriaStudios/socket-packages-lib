﻿using System;
using System.Collections.Generic;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    internal class PipelineBuilder : IPipelineBuilder
    {
        private readonly IList<Func<IIoCService, IMiddleware>> _middlewares;

        internal PipelineBuilder()
        {
            _middlewares = new List<Func<IIoCService, IMiddleware>>();
        }
        
        public IPipelineBuilder Use(Func<IIoCService, IMiddleware> middleware)
        {
            _middlewares.Add(middleware);
            
            return this;
        }
        
        public IPipelineBuilder Use(Func<IMiddleware> middleware)
        {
            _middlewares.Add(service => middleware());
            
            return this;
        }

        public IPipelineBuilder Use(IMiddleware middleware)
        {
            _middlewares.Add(service => middleware);
            
            return this;
        }

        public IPipelineBuilder Use<T>() where T : IMiddleware, new()
        {
            _middlewares.Add(service => new T());

            return this;
        }

        public IPipelineBuilder Use(MiddlewareDelegate forward, MiddlewareDelegate backward)
        {
            _middlewares.Add(service => new MiddlewareWrapper(forward, backward, service));
            
            return this;
        }

        public IPipelineBuilder Use(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegate backward)
        {
            _middlewares.Add(service => new MiddlewareWrapper(forward, backward, service));
            
            return this;
        }

        public IPipelineBuilder Use(MiddlewareDelegate forward, MiddlewareDelegateWithoutIoC backward)
        {
            _middlewares.Add(service => new MiddlewareWrapper(forward, backward, service));
            
            return this;
        }

        public IPipelineBuilder Use(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegateWithoutIoC backward)
        {
            _middlewares.Add(service => new MiddlewareWrapper(forward, backward, service));
            
            return this;
        }

        public IMiddleware Build(IIoCService ioCService) => new MiddlewarePipeline(_middlewares, ioCService);
    }
}
