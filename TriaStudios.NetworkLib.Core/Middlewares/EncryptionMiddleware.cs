﻿using System.Collections.Concurrent;
using System.IO;
using System.Security.Cryptography;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    public class EncryptionMiddleware : IMiddleware
    {
        private readonly RSACryptoServiceProvider _providerEncrypt;
        private readonly RSACryptoServiceProvider _providerDecrypt;
        private readonly ConcurrentDictionary<NetworkSession, byte[]> _sessionPublicKeys; 

        public EncryptionMiddleware()
        {
            _providerEncrypt = new RSACryptoServiceProvider();
            _providerDecrypt = new RSACryptoServiceProvider();
            _sessionPublicKeys = new ConcurrentDictionary<NetworkSession, byte[]>();
        }

        public byte[] Forward(NetworkSession session, byte[] data)
        {
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms))
            {
                var isCrypt = _sessionPublicKeys.ContainsKey(session);
                bw.Write((byte)(isCrypt ? 1 : 0));
                var exportCspBlob = _providerDecrypt.ExportCspBlob(false);
                bw.Write(exportCspBlob.Length);
                bw.Write(exportCspBlob);
                if (!isCrypt)
                {
                    bw.Write(data.Length);
                    bw.Write(data);
                }
                else
                {
                    _providerEncrypt.ImportCspBlob(_sessionPublicKeys[session]);
                    var encrypt = _providerEncrypt.Encrypt(data, false);
                    bw.Write(encrypt.Length);
                    bw.Write(encrypt);
                }

                return ms.ToArray();
            }
        }

        public byte[] Backward(NetworkSession session, byte[] data)
        {
            using (var ms = new MemoryStream(data))
            using (var br = new BinaryReader(ms))
            {
                var isCrypt = br.ReadBoolean();
                var publicKeyLength = br.ReadInt32();
                var publicKey = br.ReadBytes(publicKeyLength);
                if (!_sessionPublicKeys.ContainsKey(session))
                    _sessionPublicKeys.AddOrUpdate(session, publicKey, (networkSession, s) => s);

                var encryptDataLength = br.ReadInt32();
                var encryptData = br.ReadBytes(encryptDataLength);
                return isCrypt ? _providerDecrypt.Decrypt(encryptData, false) : encryptData;
            }
        }
    }
}
