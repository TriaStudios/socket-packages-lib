﻿using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Middlewares
{
    internal class MiddlewareWrapper : IMiddleware
    {
        private readonly MiddlewareDelegate _middlewareForward;
        private readonly MiddlewareDelegate _middlewareBackward;
        private readonly MiddlewareDelegateWithoutIoC _middlewareForwardWithoutIoC;
        private readonly MiddlewareDelegateWithoutIoC _middlewareBackwardWithoutIoC;

        private readonly IIoCService _ioCService;

        public MiddlewareWrapper(MiddlewareDelegate forward, MiddlewareDelegate backward, IIoCService ioCService)
        {
            _middlewareForward = forward;
            _middlewareBackward = backward;
            _ioCService = ioCService;
        }
        
        public MiddlewareWrapper(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegate backward, IIoCService ioCService)
        {
            _middlewareForwardWithoutIoC = forward;
            _middlewareBackward = backward;
            _ioCService = ioCService;
        }
        
        public MiddlewareWrapper(MiddlewareDelegate forward, MiddlewareDelegateWithoutIoC backward, IIoCService ioCService)
        {
            _middlewareForward = forward;
            _middlewareBackwardWithoutIoC = backward;
            _ioCService = ioCService;
        }
        
        public MiddlewareWrapper(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegateWithoutIoC backward, IIoCService ioCService)
        {
            _middlewareForwardWithoutIoC = forward;
            _middlewareBackwardWithoutIoC = backward;
            _ioCService = ioCService;
        }

        public byte[] Forward(NetworkSession session, byte[] data) =>
            _middlewareForwardWithoutIoC != null
                ? _middlewareForwardWithoutIoC.Invoke(session, data)
                : _middlewareForward.Invoke(session, data, _ioCService);

        public byte[] Backward(NetworkSession session, byte[] data) => 
            _middlewareBackwardWithoutIoC != null
                ? _middlewareBackwardWithoutIoC.Invoke(session, data)
                : _middlewareBackward.Invoke(session, data, _ioCService);
    }
}
