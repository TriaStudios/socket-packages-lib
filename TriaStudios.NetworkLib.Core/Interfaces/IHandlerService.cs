﻿using System;
using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    internal interface IHandlerService
    {
        IHandler GetHandler<TSession>(Type packetType, INetworkSocket<TSession> networkSocket) 
            where TSession : NetworkSession;
    }
}
