﻿namespace TriaStudios.NetworkLib.Core.Interfaces
{
    /// <summary>
    /// Serializer of package
    /// </summary>
    public interface ISerializer
    {
        byte[] Serialize(object model, IResolver resolver);
        object Deserialize(byte[] data, IResolver resolver);
    }
}
