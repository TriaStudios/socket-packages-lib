﻿using System;
using System.Collections.Generic;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    public interface IPackagesService
    {
        byte[] GetBytes<T>(T model);
        object GetModel(byte[] data);
        
        IEnumerable<Type> Packages { get; }
        IEnumerable<Type> Structs { get; }
    }
}
