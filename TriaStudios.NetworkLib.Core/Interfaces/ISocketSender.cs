﻿using System.Net.Sockets;
using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    internal interface ISocketSender<out T> : INetworkSocket<T> where T : NetworkSession
    {
        /// <summary>
        /// Send data over the connection
        /// </summary>
        /// <param name="client">Data and write socket connection</param>
        void Send(SocketAsyncEventArgs client);
    }
}
