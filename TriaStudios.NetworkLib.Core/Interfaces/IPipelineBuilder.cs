﻿using System;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    public interface IPipelineBuilder
    {
        IPipelineBuilder Use(Func<IIoCService, IMiddleware> middleware);
        IPipelineBuilder Use(Func<IMiddleware> middleware);
        IPipelineBuilder Use(IMiddleware middleware);
        IPipelineBuilder Use<T>() where T : IMiddleware, new();
        IPipelineBuilder Use(MiddlewareDelegate forward, MiddlewareDelegate backward);
        IPipelineBuilder Use(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegate backward);
        IPipelineBuilder Use(MiddlewareDelegate forward, MiddlewareDelegateWithoutIoC backward);
        IPipelineBuilder Use(MiddlewareDelegateWithoutIoC forward, MiddlewareDelegateWithoutIoC backward);
        IMiddleware Build(IIoCService ioCService);
    }
}
