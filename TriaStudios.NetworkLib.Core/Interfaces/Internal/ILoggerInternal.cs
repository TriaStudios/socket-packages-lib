﻿using System;
using TriaStudios.NetworkLib.Core.Enums;

namespace TriaStudios.NetworkLib.Core.Interfaces.Internal
{
    public interface ILoggerInternal : ILogger
    {
        void RegisterLogger(Action<LoggerLayer, string> onDebug);
    }
}
