﻿namespace TriaStudios.NetworkLib.Core.Interfaces.Internal
{
    internal interface IGenerateSerializers
    {
        string Generate();
    }
}
