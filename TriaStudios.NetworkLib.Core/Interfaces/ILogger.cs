﻿using TriaStudios.NetworkLib.Core.Enums;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    public interface ILogger
    {
        void Debug(LoggerLayer layer, string message);
    }
}
