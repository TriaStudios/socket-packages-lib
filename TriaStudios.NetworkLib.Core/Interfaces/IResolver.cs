﻿using System;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    /// <summary>
    /// Resolver for serializers of packages
    /// </summary>
    public interface IResolver
    {
        ISerializer GetSerializer(Type type);
    }
}
