﻿using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    public delegate byte[] MiddlewareDelegateWithoutIoC(NetworkSession session, byte[] data);
    public delegate byte[] MiddlewareDelegate(NetworkSession session, byte[] data, IIoCService ioCService);
    
    public interface IMiddleware
    {
        byte[] Forward(NetworkSession session, byte[] data);
        byte[] Backward(NetworkSession session, byte[] data);
    }
    
    public abstract class Middleware<T> : IMiddleware
        where T : NetworkSession
    {
        public abstract byte[] Forward(T session, byte[] data);
        public abstract byte[] Backward(T session, byte[] data);

        public byte[] Forward(NetworkSession session, byte[] data) => Forward((T) session, data);
        public byte[] Backward(NetworkSession session, byte[] data) => Backward((T) session, data);
    }
}
