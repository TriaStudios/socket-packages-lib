﻿using System;
using System.Threading.Tasks;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    internal interface IHandler
    {
        Type ModelType { get; }
        Task HandleAsync(object session, object model);
        void RegisterHandler(object networkSocket);
    }
}
