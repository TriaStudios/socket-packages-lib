﻿using System.Collections.Generic;
using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    /// <summary>
    /// Interface of network connection
    /// </summary>
    public interface INetworkSocket<out T>
        where T : NetworkSession
    {
        /// <summary>
        /// Gets IoC
        /// </summary>
        /// <returns></returns>
        IIoCService IoCService { get; }

        IReadOnlyCollection<T> Sessions { get; }
    }
}
