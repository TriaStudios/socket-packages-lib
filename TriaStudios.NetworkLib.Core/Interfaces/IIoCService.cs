﻿using System;
using System.Collections.Generic;

namespace TriaStudios.NetworkLib.Core.Interfaces
{
    public interface IIoCService
    {
        T GetInstance<T>();
        object GetInstance(Type type);
        IEnumerable<T> GetAllInstances<T>();
        IEnumerable<object> GetAllInstances(Type type);
        IoCCreator<T> GetCreator<T>();
        IEnumerable<IoCCreator<T>> GetAllCreators<T>();
    }

    public sealed class IoCCreator<T>
    {
        public Func<T> Creator { get; set; }
        public Type ModelType { get; set; }
    }
}
