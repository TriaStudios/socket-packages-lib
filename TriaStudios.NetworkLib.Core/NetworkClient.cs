﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core
{
    /// <summary>
    /// Client for connection to server
    /// </summary>
    /// <typeparam name="T">Custom type session with data</typeparam>
    internal class NetworkClient<T> : NetworkSocket<T> where T : NetworkSession, new()
    {
        private T Session { get; set; }

        public override IReadOnlyCollection<T> Sessions => new[] {Session};

        internal NetworkClient(IPAddress address, int port, IIoCService ioCService, IMiddleware middleware) 
            : base(address, port, ioCService, middleware)
        {
        }

        protected override SessionSourceType SourceType => SessionSourceType.Client;

        protected override void CreateConnection()
        {
            Socket = new Socket(EndPoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            StartConnect();
        }

        private void StartConnect()
        {
            // create async socket for connection
            var connectEventArg = new SocketAsyncEventArgs();
            connectEventArg.Completed += ConnectEventArg_Completed;
            connectEventArg.RemoteEndPoint = EndPoint;
            connectEventArg.AcceptSocket = Socket;

            // start connection process
            var willRaiseEvent = Socket.ConnectAsync(connectEventArg);
            if (!willRaiseEvent)
            {
                ProcessConnect(connectEventArg);
            }
        }

        private void ConnectEventArg_Completed(object sender, SocketAsyncEventArgs e) => ProcessConnect(e);

        protected override void OnConnectInternal(T session)
        {
            Session = session;
            OnConnected?.Invoke(session);
        }

        protected override void OnDisconnectInternal(T session)
        {
            DisposeDictionaries(session);
            OnDisconnected?.Invoke(session);
        }

        #region Handlers

        internal event Action<T> OnConnected;
        internal event Action<T> OnDisconnected;

        #endregion
    }
}
