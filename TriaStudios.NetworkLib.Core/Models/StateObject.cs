﻿using System.Net.Sockets;
using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Core.Models
{
    /// <summary>
    /// State of client on server
    /// </summary>
    public class StateObject
    {
        public StateObject(SocketAsyncEventArgs read, SocketAsyncEventArgs write)
        {
            read.UserToken = this;
            read.SetBuffer(ReadBuffer, 0, BufferSize);
            ReadEventArg = read;

            write.UserToken = this;
            WriteEventArg = write;
        }

        public SocketAsyncEventArgs ReadEventArg { get; private set; }
        public SocketAsyncEventArgs WriteEventArg { get; private set; }

        public NetworkSession Session;
        public const int BufferSize = 4096;
        public readonly byte[] ReadBuffer = new byte[BufferSize];

        public int ReceivedBytes = 0;
        public int ReceivedOffset = 0;
    }

}
