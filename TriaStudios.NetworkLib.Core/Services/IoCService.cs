﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Services
{
    /// <summary>
    ///     IoC service
    /// </summary>
    internal class IoCService : IIoCService
    {
        private readonly ConcurrentDictionary<Type, IEnumerable<ServiceInternal>> _objects;

        internal IoCService()
        {
            _objects = new ConcurrentDictionary<Type, IEnumerable<ServiceInternal>>(
                new Dictionary<Type, IEnumerable<ServiceInternal>>
                {
                    {
                        typeof(IIoCService), new List<ServiceInternal>
                        {
                            new ServiceInternal
                            {
                                Lifetime = DependencyLifetime.Singleton,
                                Object = this,
                                ObjectType = typeof(IoCService)
                            }
                        }
                    }
                });
            
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Register(assembly);
            }
            
            PassDependencies();
        }

        internal void Start()
        {
            foreach (var obj in _objects.Values
                         .SelectMany(x => x)
                         .Distinct()
                         .Where(x => typeof(IoCBehaviour).IsAssignableFrom(x.ObjectType))
                         .OrderBy(x => x.ObjectType.GetCustomAttribute<OrderLoadAttribute>(false)?.Order ?? 0))
            {
                if (obj.Object == null)
                {
                    obj.CreatorPipeline?.Invoke(obj);
                }
                    
                (obj.Object as IoCBehaviour)?.Start();
            }
        }

        /// <summary>
        ///     Registration entities
        /// </summary>
        /// <param name="assembly">project with entities</param>
        private void Register(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                // check dependency attribute and existance type in IoC
                var lifetime = type.GetCustomAttribute<DependencyAttribute>(false)?.Lifetime;
                if (!type.IsAbstract && typeof(IHandler).IsAssignableFrom(type))
                {
                    lifetime = DependencyLifetime.Transient;
                }
                if (!lifetime.HasValue) continue;

                var interfaces = type.GetInterfaces();
                var count = interfaces.Length;
                var obj = new ServiceInternal
                {
                    ObjectType = type,
                    Lifetime = lifetime.Value,
                    CreatorPipeline = si => si.Object = Activator.CreateInstance(si.ObjectType)
                };
                if (count > 0)
                {
                    foreach (var @interface in interfaces)
                    {
                        _objects.AddOrUpdate(
                            @interface, 
                            new List<ServiceInternal> {obj}, 
                            (_, list) => list.Append(obj));
                    }
                }
                else
                {
                    _objects.AddOrUpdate(
                        type, 
                        new List<ServiceInternal> {obj}, 
                        (_, list) => list.Append(obj));
                }
            }
        }

        private void PassDependencies()
        {
            var serviceInternals = _objects.Values
                .SelectMany(x => x)
                .Distinct();
            foreach (var serviceInternal in serviceInternals)
            {
                var objType = serviceInternal.ObjectType;
                var fields = objType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                foreach (var fieldInfo in fields)
                {
                    var fieldType = fieldInfo.FieldType;
                    var isArray = false;
                
                    if (typeof(IEnumerable).IsAssignableFrom(fieldType))
                    {
                        fieldType = fieldType
                            .GetInterfaces()
                            .FirstOrDefault(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>))?
                            .GetGenericArguments()
                            .ElementAtOrDefault(0);
                        isArray = true;
                    }
                
                    if (fieldType == null) continue;
                
                    if (!_objects.ContainsKey(fieldType)) continue;
                    var fi = fieldInfo;
                    serviceInternal.CreatorPipeline += si =>
                    {
                        var objects = GetAllInstances(fieldType).ToArray();
                        if (isArray)
                        {
                            var length = objects.Length;
                            var arr = Array.CreateInstance(fieldType, length);
                            Array.Copy(objects, arr, length);
                            fi.SetValue(si.Object, arr);
                            return;
                        }

                        var concreteType = fi.GetCustomAttribute<ConcreteAttribute>(false)?.ConcreteType;
                        fi.SetValue(si.Object,
                            concreteType == null ? objects.FirstOrDefault() :
                            objects.FirstOrDefault(x => x.GetType() == concreteType));
                    };
                }
            }
        }

        /// <summary>
        ///     Get instance mono
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetInstance<T>() => (T) GetInstance(typeof(T));

        public object GetInstance(Type type)
        {
            if (!_objects.TryGetValue(type, out var obj))
            {
                throw new KeyNotFoundException($"Service '{type.Name}' is not registered");
            }

            var serviceInternal = obj.FirstOrDefault();
            return InitService(serviceInternal)?.Object;
        }

        public IEnumerable<T> GetAllInstances<T>() => GetAllInstances(typeof(T)).Cast<T>();

        public IEnumerable<object> GetAllInstances(Type type)
        {
            if (!_objects.TryGetValue(type, out var obj))
            {
                throw new KeyNotFoundException($"Service '{type.Name}' is not registered");
            }

            return obj.Select(InitService).Select(x => x.Object);
        }

        public IoCCreator<T> GetCreator<T>()
        {
            var type = typeof(T);
            if (!_objects.TryGetValue(type, out var obj))
            {
                throw new KeyNotFoundException($"Service '{type.Name}' is not registered");
            }

            var serviceInternal = obj.FirstOrDefault();
            
            return new IoCCreator<T>
            {
                Creator = () => (T)InitService(serviceInternal)?.Object,
                ModelType = serviceInternal?.ObjectType
            };
        }

        public IEnumerable<IoCCreator<T>> GetAllCreators<T>()
        {
            var type = typeof(T);
            if (!_objects.TryGetValue(type, out var obj))
            {
                throw new KeyNotFoundException($"Service '{type.Name}' is not registered");
            }

            return obj.Select(x => new IoCCreator<T>
            {
                Creator = () => (T)InitService(x).Object,
                ModelType = x.ObjectType
            });
        }

        private static ServiceInternal InitService(ServiceInternal serviceInternal)
        {
            if (serviceInternal?.Object == null)
            {
                serviceInternal?.CreatorPipeline?.Invoke(serviceInternal);
                return serviceInternal;
            }

            if (serviceInternal.Lifetime != DependencyLifetime.Transient) return serviceInternal;
            
            serviceInternal.CreatorPipeline?.Invoke(serviceInternal);
            return serviceInternal;
        }

        private class ServiceInternal
        {
            public DependencyLifetime Lifetime { get; set; }
            public object Object { get; set; }
            public Type ObjectType { get; set; }
            public Action<ServiceInternal> CreatorPipeline { get; set; }
        }
    }
}
