﻿using System;
using System.Collections.Generic;
using System.Linq;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Services
{
    /// <summary>
    ///     Handler service
    /// </summary>
    [Dependency]
    [OrderLoad(int.MinValue + 2)]
    internal class HandlerService : IoCBehaviour, IHandlerService
    {
        private readonly IIoCService _ioCService;
        
        /// <summary>
        ///     All handlers
        /// </summary>
        private readonly Dictionary<Type, Func<IHandler>> _handlerServices;

        public HandlerService()
        {
            _handlerServices = new Dictionary<Type, Func<IHandler>>();
        }

        public override void Start() => RegisterHandlers();

        /// <summary>
        ///     Registration handlers
        /// </summary>
        private void RegisterHandlers()
        {
            var objs = _ioCService.GetAllCreators<IHandler>();
            foreach (var obj in objs)
            {
                _handlerServices.Add(obj.ModelType.BaseType?.GetGenericArguments().ElementAtOrDefault(0), obj.Creator);
            }
        }

        /// <summary>
        ///     Get handler of package
        /// </summary>
        /// <param name="packetType">package number</param>
        /// <param name="networkSocket">network socket</param>
        /// <returns></returns>
        public IHandler GetHandler<TSession>(Type packetType, INetworkSocket<TSession> networkSocket) 
            where TSession : NetworkSession
        {
            _handlerServices.TryGetValue(packetType, out var value);
            var handler = value?.Invoke();
            handler?.RegisterHandler(networkSocket);
            return handler;
        }
    }
}
