﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Interfaces.Internal;

namespace TriaStudios.NetworkLib.Core.Services
{
    [Dependency]
    [OrderLoad(int.MinValue + 1)]
    internal class ResolverService : IoCBehaviour, IResolver
    {
        private readonly ILogger _logger;
        private readonly IGenerateSerializers _generateSerializers;
        
        private readonly Dictionary<Type, ISerializer> _typeToSerializer;

        public ResolverService()
        {
            _typeToSerializer = new Dictionary<Type, ISerializer>();
        }

        public override void Start()
        {
            _logger.Debug(LoggerLayer.Debug, "Roslyn compiling packages...");
            var sw = new Stopwatch();
            sw.Start();
            var types = _generateSerializers.Generate();
            using (var ms = new MemoryStream())
            {
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                var portableExecutableReferences = assemblies
                    .Where(x => !string.IsNullOrEmpty(x.Location))
                    .Select(x => MetadataReference.CreateFromFile(x.Location));
                var cSharpCompilationOptions = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary);
                var syntaxTrees = new[] {CSharpSyntaxTree.ParseText(types)};
                var result = CSharpCompilation.Create("GENERATED_ASSEMBLY", 
                    syntaxTrees, portableExecutableReferences, cSharpCompilationOptions).Emit(ms);
                if (!result.Success)
                {
                    var failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);

                    foreach (var diagnostic in failures)
                    {
                        _logger.Debug(LoggerLayer.Error, diagnostic.ToString());
                    }
                }
                else
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    var compiledAssembly = Assembly.Load(ms.ToArray());
                    foreach (var serializerType in compiledAssembly.GetTypes())
                    {
                        var typeModel = serializerType.BaseType?.GenericTypeArguments[0];
                        var serializer = (ISerializer) Activator.CreateInstance(serializerType);
                        _typeToSerializer[typeModel] = serializer;
                    }
                }
            }
            sw.Stop();
            _logger.Debug(LoggerLayer.Debug, $"Roslyn compiled successful for {sw.Elapsed}!");
        }

        public ISerializer GetSerializer(Type type)
        {
            if (!_typeToSerializer.TryGetValue(type, out var value))
            {
                throw new Exception("Serializer is not existed");
            }
            return value;
        }
    }
}
