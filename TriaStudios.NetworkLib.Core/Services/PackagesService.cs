﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Core.Services
{
    /// <summary>
    /// Packages and its numbers assotiations
    /// </summary>
    [Dependency]
    [OrderLoad(int.MinValue)]
    internal class PackagesService : IoCBehaviour, IPackagesService
    {
        private readonly IResolver _resolver;
        
        private readonly Dictionary<short, Type> _codeToType = new Dictionary<short, Type>();
        private readonly Dictionary<Type, short> _typeToCode = new Dictionary<Type, short>();
        private readonly List<Type> _structs = new List<Type>();

        public IEnumerable<Type> Packages => _typeToCode.Keys;
        public IEnumerable<Type> Structs => _structs.ToArray();

        public override void Start() => Register();

        private void Register()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Register(assembly);
            }
        }
        
        private void Register(Assembly assembly)
        {
            foreach (var item in assembly.GetTypes())
            {
                if (item.GetCustomAttribute<PackagePackAttribute>() == null) continue;
                var code = item.GetCustomAttribute<PackagePackAttribute>().Code;
                if (code == -1)
                {
                    _structs.Add(item);
                    continue;
                }
                _codeToType.Add(code, item);
                _typeToCode.Add(item, code);
            }
        }

        public byte[] GetBytes<T>(T model)
        {
            if (!_typeToCode.TryGetValue(typeof(T), out var code))
                return Array.Empty<byte>();
            using (var ms = new MemoryStream())
            using (var bw = new BinaryWriter(ms)) 
            {
                bw.Write(code);

                bw.Write(_resolver.GetSerializer(typeof(T)).Serialize(model, _resolver));
                return ms.ToArray();
            }
        }

        public object GetModel(byte[] data)
        {
            using (var ms = new MemoryStream(data))
            using (var br = new BinaryReader(ms))
            {
                var code = br.ReadInt16();
                return !_codeToType.TryGetValue(code, out var type)
                    ? null
                    : _resolver.GetSerializer(type).Deserialize(br.ReadBytes(data.Length - 2), _resolver);
            }
        }
    }
}
