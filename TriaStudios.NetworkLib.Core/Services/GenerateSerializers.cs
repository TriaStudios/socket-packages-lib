﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Interfaces.Internal;

namespace TriaStudios.NetworkLib.Core.Services
{
    [Dependency]
    internal class GenerateSerializers : IGenerateSerializers
    {
        private readonly IPackagesService _packagesService;
        
        public string Generate()
        {
            var classes = _packagesService.Packages
                .Union(_packagesService.Structs)
                .Select(GenerateFile)
                .ToList();

            var file = "";
            file += "using System.IO;\n";
            file += "using TriaStudios.NetworkLib.Core.Abstract;\n";
            file += "using TriaStudios.NetworkLib.Core.Interfaces;\n";
            file += "\n";
            file += "namespace GENERATED {\n";
            file += string.Join("\n", classes);
            file += "\n}";
            return file;
        }

        private static string GenerateFile(Type type)
        {
            var attribute = typeof(IgnoreAttribute);
            var file = "";
            
            file += $"\tpublic class {type.Name}Serializer : Serializer<{type.FullName}>\n";
            file += "\t{\n";
            file += $"\t\tpublic override {type.FullName} Deserialize(byte[] data, IResolver resolver)\n";
            file += "\t\t{\n";
            file += "\t\t\tusing (var ms = new MemoryStream(data))\n";
            file += "\t\t\tusing (var br = new BinaryReader(ms))\n";
            file += "\t\t\t{\n";
            file += $"\t\t\t\t{type.FullName} model = new {type.FullName}();\n";

            foreach (var propertyInfo in type.GetProperties().Where(x => x.GetCustomAttribute(attribute) == null))
            {
                if (propertyInfo.PropertyType.IsPrimitive || propertyInfo.PropertyType == typeof(string))
                {
                    file +=
                        $"\t\t\t\tmodel.{propertyInfo.Name} = {DeserializePrimitiveType(propertyInfo.PropertyType)};\n";
                }
                else if (propertyInfo.PropertyType.IsEnum)
                {
                    file +=
                        $"\t\t\t\tmodel.{propertyInfo.Name} = ({propertyInfo.PropertyType.FullName}){DeserializePrimitiveType(propertyInfo.PropertyType)};\n";
                }
                else
                {
                    file += "\t\t\t\t{\n";
                    file += DeserializeNonePrimitiveType(propertyInfo.PropertyType, $"model.{propertyInfo.Name}", 1);
                    file += "\t\t\t\t}\n";
                }
            }
            file += "\t\t\t\treturn model;\n";
            file += "\t\t\t}\n";
            file += "\t\t}\n\n";

            file += $"\t\tpublic override byte[] Serialize({type.FullName} model, IResolver resolver)\n";
            file += "\t\t{\n";
            file += "\t\t\tusing (var ms = new MemoryStream())\n";
            file += "\t\t\tusing (var bw = new BinaryWriter(ms))\n";
            file += "\t\t\t{\n";

            foreach (var propertyInfo in type.GetProperties().Where(x => x.GetCustomAttribute(attribute) == null))
            {
                if (propertyInfo.PropertyType.IsPrimitive || propertyInfo.PropertyType == typeof(string))
                {
                    file += $"\t\t\t\tbw.Write(model.{propertyInfo.Name});\n";
                }
                else if (propertyInfo.PropertyType.IsEnum)
                {
                    file += $"\t\t\t\tbw.Write(({propertyInfo.PropertyType.GetField("value__").FieldType.FullName})model.{propertyInfo.Name});\n";
                }
                else
                {
                    file += "\t\t\t\t{\n";
                    file += $"{SerializeNonePrimitiveType(propertyInfo.PropertyType, $"model.{propertyInfo.Name}", 1)}";
                    file += "\t\t\t\t}\n";
                }
            }
            file += "\t\t\t\treturn ms.ToArray();\n";
            file += "\t\t\t}\n";
            file += "\t\t}\n";
            file += "\t}\n";
            return file;
        }

        private static string DeserializePrimitiveType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                    return "br.ReadByte()";
                case TypeCode.Int16:
                    return "br.ReadInt16()";
                case TypeCode.UInt16:
                    return "br.ReadUInt16()";
                case TypeCode.Int32:
                    return "br.ReadInt32()";
                case TypeCode.UInt32:
                    return "br.ReadUInt32()";
                case TypeCode.Int64:
                    return "br.ReadInt64()";
                case TypeCode.UInt64:
                    return "br.ReadUInt64()";
                case TypeCode.Single:
                    return "br.ReadSingle()";
                case TypeCode.Double:
                    return "br.ReadDouble()";
                case TypeCode.String:
                    return "br.ReadString()";
                case TypeCode.Boolean:
                    return "br.ReadBoolean()";
                case TypeCode.Char:
                case TypeCode.DateTime:
                case TypeCode.DBNull:
                case TypeCode.Decimal:
                case TypeCode.Empty:
                case TypeCode.Object:
                case TypeCode.SByte:
                default:
                    throw new NotSupportedException("This type of primitive is not supported");
            }
        }

        private static string SerializeNonePrimitiveType(Type type, string propertyName, int numInputs)
        {
            var res = "";
            var tabulations = "\t\t\t\t\t";
            for (var i = 0; i < numInputs - 1; i++)
            {
                tabulations += "\t";
            }

            if (type.IsEnum)
            {
                return tabulations + $"bw.Write({propertyName});\n";
            }

            if (type.GetInterface(nameof(IDictionary)) != null)
            {
                return res;
            }

            if (type.GetInterface(nameof(IEnumerable)) != null)
            {
                var typeList = type.GetGenericArguments()[0];
                res += tabulations + $"bw.Write({propertyName}.Count);\n";
                res += tabulations + $"foreach (var subModel{numInputs} in {propertyName})\n";

                res += tabulations + "{\n";
                if (typeList.IsPrimitive || typeList == typeof(string))
                {
                    res += tabulations + $"\tbw.Write(subModel{numInputs});\n";
                }
                else if (typeList.IsEnum)
                {
                    res += tabulations + $"\tbw.Write(({typeList.GetField("value__").FieldType.FullName})subModel{numInputs});\n";
                }
                else
                {
                    res += SerializeNonePrimitiveType(typeList, $"subModel{numInputs}", numInputs + 1);
                }
                res += tabulations + "}\n";
                return res;
            }

            res += tabulations + $"byte[] bytes{numInputs} = resolver.GetSerializer(typeof({type})).Serialize({propertyName}, resolver);\n";
            res += tabulations + $"bw.Write(bytes{numInputs}.Length);\n";
            res += tabulations + $"bw.Write(bytes{numInputs});\n";

            return res;
        }

        private static string DeserializeNonePrimitiveType(Type type, string propertyName, int numInputs)
        {
            var res = "";
            var tabulations = "\t\t\t\t\t";
            for (var i = 0; i < numInputs - 1; i++)
            {
                tabulations += "\t";
            }

            if (type.IsEnum)
            {
                return tabulations + $"";
            }

            if (type.GetInterface(nameof(IEnumerable)) != null)
            {
                var typeList = type.GetGenericArguments()[0];
                res += tabulations + $"int count{numInputs};\n";
                res += tabulations + $"count{numInputs} = br.ReadInt32();\n";
                res += tabulations + $"{GetFullName(type)} collection{numInputs} = new {GetFullName(type)}();\n";

                res += tabulations + $"for (int i{numInputs} = 0; i{numInputs} < count{numInputs}; i{numInputs}++)\n";
                res += tabulations + "{\n";

                if (typeList.IsPrimitive || typeList == typeof(string))
                {
                    res += tabulations + "\t" + $"collection{numInputs}.Add({DeserializePrimitiveType(typeList)});\n";
                }
                else if (typeList.IsEnum)
                {
                    res += tabulations + "\t" + $"collection{numInputs}.Add(({typeList.FullName}){DeserializePrimitiveType(typeList)});\n";
                }
                else if (typeList.GetInterface(nameof(IEnumerable)) != null)
                {
                    res += DeserializeNonePrimitiveType(typeList, $"collection{numInputs}", numInputs + 1);
                }
                else
                {
                    res += tabulations + "\t" + $"int size{numInputs} = br.ReadInt32();\n";
                    res += tabulations + "\t" + $"{typeList.FullName} subModel{numInputs} = ({typeList.FullName})resolver.GetSerializer(typeof({typeList.FullName})).Deserialize(br.ReadBytes(size{numInputs}), resolver);\n";
                    res += tabulations + "\t" + $"collection{numInputs}.Add(subModel{numInputs});\n";
                }

                res += tabulations + "}\n";
                if (numInputs == 1)
                    res += tabulations + $"{propertyName} = collection{numInputs};\n";
                else
                    res += tabulations + $"{propertyName}.Add(collection{numInputs});\n";
                return res;
            }

            res += tabulations + $"int size{numInputs} = br.ReadInt32();\n";
            res += tabulations + $"{type.FullName} subModel{numInputs} = ({type.FullName})resolver.GetSerializer(typeof({type.FullName})).Deserialize(br.ReadBytes(size{numInputs}), resolver);\n";
            res += tabulations + $"{propertyName} = subModel{numInputs};\n";

            return res;
        }

        private static string GetFullName(Type type)
        {
            var ans = type.FullName?.Split('`')[0];
            if (type.GetGenericArguments().Length <= 0) return ans;
            
            ans += "<";
            ans += string.Join(",", type.GetGenericArguments().Select(x => GetFullName(x)));
            ans += ">";

            return ans;
        }
    }
}
