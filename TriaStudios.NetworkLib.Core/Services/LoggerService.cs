﻿using System;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces.Internal;

namespace TriaStudios.NetworkLib.Core.Services
{
    [Dependency]
    internal class LoggerService : ILoggerInternal
    {
        private Action<LoggerLayer, string> _onDebug;

        public void RegisterLogger(Action<LoggerLayer, string> onDebug)
        {
            _onDebug += onDebug;   
        }

        public void Debug(LoggerLayer layer, string message)
        {
            _onDebug?.Invoke(layer, message);
        }
    }
}
