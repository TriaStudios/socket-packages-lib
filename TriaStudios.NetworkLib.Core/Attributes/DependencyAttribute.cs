﻿using System;
using TriaStudios.NetworkLib.Core.Enums;

namespace TriaStudios.NetworkLib.Core.Attributes
{
    /// <summary>
    ///     Entity for IoC
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class DependencyAttribute : Attribute
    {
        public DependencyAttribute(DependencyLifetime lifetime = DependencyLifetime.Singleton)
        {
            Lifetime = lifetime;
        }

        public DependencyLifetime Lifetime { get; set; }
    }
}
