﻿using System;

namespace TriaStudios.NetworkLib.Core.Attributes
{
    /// <summary>
    /// For serialization class 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum)]
    public class PackagePackAttribute : Attribute
    {
        public short Code { get; set; }

        public PackagePackAttribute(short code = -1)
        {
            Code = code;
        }
    }
}
