﻿using System;

namespace TriaStudios.NetworkLib.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum)]
    public class OrderLoadAttribute : Attribute
    {
        public OrderLoadAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; set; }
    }
}
