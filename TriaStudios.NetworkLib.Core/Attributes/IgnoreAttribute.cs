﻿using System;

namespace TriaStudios.NetworkLib.Core.Attributes
{
    /// <summary>
    /// Ignore for serialization of package's properties
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class IgnoreAttribute : Attribute
    {
    }
}
