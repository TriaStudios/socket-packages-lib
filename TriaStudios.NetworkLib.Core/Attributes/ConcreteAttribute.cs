﻿using System;

namespace TriaStudios.NetworkLib.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Enum)]
    public class ConcreteAttribute : Attribute
    {
        public Type ConcreteType { get; set; }

        public ConcreteAttribute(Type concreteType)
        {
            ConcreteType = concreteType;
        }
    }
}
