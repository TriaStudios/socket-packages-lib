﻿namespace TriaStudios.NetworkLib.Core.Enums
{
    public enum LoggerLayer
    {
        Trace,
        Debug,
        Info,
        Warning,
        Error
    }
}
