﻿namespace TriaStudios.NetworkLib.Core.Enums
{
    public enum DependencyLifetime
    {
        Transient,
        Singleton
    }
}
