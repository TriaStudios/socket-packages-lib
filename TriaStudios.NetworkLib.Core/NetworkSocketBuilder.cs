﻿using System;
using System.Net;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Interfaces.Internal;
using TriaStudios.NetworkLib.Core.Middlewares;
using TriaStudios.NetworkLib.Core.Services;

namespace TriaStudios.NetworkLib.Core
{
    public class NetworkSocketBuilder<T>
        where T : NetworkSession, new()
    {
        private IPEndPoint _endPoint;
        private Action<LoggerLayer, string> _onLogFunction;
        private Action<T> _onConnected;
        private Action<T> _onDisconnected;
        private readonly IPipelineBuilder _pipelineBuilder;

        internal NetworkSocketBuilder()
        {
            _pipelineBuilder = new PipelineBuilder();
        }

        public NetworkSocketBuilder<T> BindAddress(IPEndPoint ipEndPoint)
        {
            _endPoint = ipEndPoint;

            return this;
        }

        public NetworkSocketBuilder<T> AddLogger(Action<LoggerLayer, string> debug)
        {
            _onLogFunction += debug;

            return this;
        }

        public NetworkSocketBuilder<T> AddConnectHandler(Action<T> action)
        {
            _onConnected += action;

            return this;
        }

        public NetworkSocketBuilder<T> AddDisconnectHandler(Action<T> action)
        {
            _onDisconnected += action;

            return this;
        }

        public NetworkSocketBuilder<T> AddConnectHandler(Action<T, INetworkSocket<T>> action)
        {
            _onConnected += session => action(session, (INetworkSocket<T>)session.Socket);

            return this;
        }

        public NetworkSocketBuilder<T> AddDisconnectHandler(Action<T, INetworkSocket<T>> action)
        {
            _onDisconnected += session => action(session, (INetworkSocket<T>)session.Socket);

            return this;
        }

        public NetworkSocketBuilder<T> AddPipeline(Action<IPipelineBuilder> builderAction)
        {
            builderAction(_pipelineBuilder);
            return this;
        }

        public NetworkSocket<T> BuildServer()
        {
            if (_endPoint == null)
            {
                throw new ArgumentNullException(nameof(_endPoint), "Call BindAddress to setting");
            }
            
            var ioCService = RegisterServices();
            ioCService.Start();

            var server = new NetworkServer<T>(
                _endPoint.Address, 
                _endPoint.Port,
                ioCService,
                _pipelineBuilder.Build(ioCService));
            server.OnConnected += _onConnected;
            server.OnDisconnected += _onDisconnected;
            
            return server;
        }

        public NetworkSocket<T> BuildClient()
        {
            if (_endPoint == null)
            {
                throw new ArgumentNullException(nameof(_endPoint), "Call BindAddress to setting");
            }
            
            var ioCService = RegisterServices();
            ioCService.Start();

            var client = new NetworkClient<T>(
                _endPoint.Address, 
                _endPoint.Port,
                ioCService,
                _pipelineBuilder.Build(ioCService));
            client.OnConnected += _onConnected;
            client.OnDisconnected += _onDisconnected;
            
            return client;
        }

        private IoCService RegisterServices()
        {
            var ioCService = new IoCService();
            ioCService.GetInstance<ILoggerInternal>().RegisterLogger(_onLogFunction);
            return ioCService;
        }
    }
}
