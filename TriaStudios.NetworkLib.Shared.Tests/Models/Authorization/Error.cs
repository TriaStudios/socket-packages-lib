﻿using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Shared.Tests.Enums;

namespace TriaStudios.NetworkLib.Shared.Tests.Models.Authorization
{
    [PackagePack((short)PackageType.Error)]
    public class Error
    {
        public ErrorType ErrorCode { get; set; }
    }
}
