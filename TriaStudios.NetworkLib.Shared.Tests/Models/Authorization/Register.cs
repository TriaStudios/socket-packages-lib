﻿using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Shared.Tests.Enums;

namespace TriaStudios.NetworkLib.Shared.Tests.Models.Authorization
{
    [PackagePack((short)PackageType.Register)]
    public class Register
    {
        public string Name { get; set; }
    }
}
