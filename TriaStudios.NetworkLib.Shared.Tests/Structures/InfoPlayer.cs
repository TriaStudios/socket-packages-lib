﻿using TriaStudios.NetworkLib.Core.Attributes;
using System.Collections.Generic;

namespace TriaStudios.NetworkLib.Shared.Tests.Structures
{
    [PackagePack]
    public class InfoPlayer
    {
        public List<List<int>> MyProperty { get; set; }
    }
}
