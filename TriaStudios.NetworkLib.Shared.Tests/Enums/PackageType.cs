﻿namespace TriaStudios.NetworkLib.Shared.Tests.Enums
{
    public enum PackageType
    {
        Welcome = 1,
        Register = 2,
        Error = 3,
    }
}
