﻿namespace TriaStudios.NetworkLib.Shared.Tests.Enums
{
    public enum ErrorType : ulong
    {
        RegisterExistUser = 1,
        RegisterLongUsername = 2,
        RegisterShortUsername = 3,
    }
}
