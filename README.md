# Socket packages lib
## Setting new server/client
1. Create session data
```c#
public class SessionData
{
    //Data
}
```
2. Create custom session
```c#
public class CustomSession : NetworkSession
{
    public SessionData Data { get; } = new GameSessionData();
}
```
3. Build server
```c#
public class Program
{
    private static readonly IPEndPoint Ip = IPEndPoint.Parse("127.0.0.1:1234");

    public static async Task Main(string[] args)
    {
        var builderSocket = NetworkSocket<GameSession>.Builder()
            .BindAddress(Ip)
            .AddLogger((layer, s) => Console.WriteLine($"[{DateTime.Now}] [{layer.ToString().ToUpper()}] {s}"))
            .AddConnectHandler((session, services) =>
            {
                var instance = services.GetInstance<ILogger>();
                instance.Debug(LoggerLayer.Debug, $"Connected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
            })
            .AddDisconnectHandler((session, services) =>
            {
                var instance = services.GetInstance<ILogger>();
                instance.Debug(LoggerLayer.Debug, $"Disconnected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
            })
            .AddPipeline(builder =>
            {
                // If you use encryption (RSA) it is useless to use compression
                // because encryption text is not to be able to compress more than it is.
                builder.UseEncryption();
                builder.UseCompression(CompressionLevel.Optimal);
                
                // Custom middlewares
                builder.Use(service => new CustomMiddleware1(service));
                builder.Use<CustomMiddleware2>();
            });

        // Build and run server from socket builder
        var server = builderSocket
             // You can register connect/disconnect handlers with IoCService
            .RegisterConnectHandler((session, service) => 
                service.GetInstance<HelloWorldFactory>().Send(session))
            .BuildServer();
        await server.RunAsync();
    }
}
```
4. Build client

You can build client right like server.
```c#
public class Program
{
    private static readonly IPEndPoint Ip = IPEndPoint.Parse("127.0.0.1:1234");

    public static async Task Main(string[] args)
    {
        // ... the same creator like server ...

        var client = builderSocket.BuildClient();
        await client.RunAsync();
    }
}
```
5. Create custom dependencies
```c#
/// <summary>
/// All dependencies are singleton by default!
/// </summary>
[Dependency]
[OrderLoad(100)]
public class Service1 : IoCBehaviour
{
    private readonly ILogger _logger;

    public override void Start()
    {
        _logger.Debug(LoggerLayer.Info, "Here you can run background processes");
    }
}
[Dependency(DependencyLifetime.Transient)]
public class Service2
{
    // You can pass dependencies from IoC like that
    private readonly Service1 _hostedService;
}
```
6. Create custom middlewares
```c#
public class CustomMiddleware1 : IMiddleware
{
    private readonly IoCService _ioCService;

    public CustomMiddleware1(IoCService ioCService)
    {
        _ioCService = ioCService;
    }

    public byte[] Forward(NetworkSession session, byte[] data)
    {
        return data;
    }

    public byte[] Backward(NetworkSession session, byte[] data)
    {
        return data;
    }
}

public class CustomMiddleware2 : IMiddleware
{
    public byte[] Forward(NetworkSession session, byte[] data)
    {
        return data;
    }

    public byte[] Backward(NetworkSession session, byte[] data)
    {
        return data;
    }
}
```

## How do new package create?
1. Add new type and number of package.
```c#
public enum PackageType
{
    // ...
    HelloWorldType = 16,
    // ...
}
```
2. Create model class.
```c#
[PackagePack((short)PackageType.HelloWorldType)]
public class HelloWorldModel
{
    public InnerStructure InnerStructure { get; set; }
}

[PackagePack]
public class InnerStructure
{
    public int Number { get; set; }
}
```
3. Create handler
```c#
/// <summary>
/// Class inheriting from Handler is automatic TRANSIENT dependency!
/// It is no need to write [Dependency(DependencyLifetime.Transient)]
/// </summary>
public class HelloWorldHandler : Handler<HelloWorldModel, GameSession>
{
    private readonly ILogger _logger;

    protected override void Handle(GameSession clientData, HelloWorldModel model)
    {
        _logger.Debug(LoggerLayer.Info, "Here you can handle package and send other packages backwards");
        _logger.Debug(LoggerLayer.Info, $"Receive message from {clientData.ToInfo.EndPoint}");

        // property ConnectedSessions can help you for handling package on server side
    }
}
```
4. Create factory
```c#
[Dependency]
public class HelloWorldFactory
{
    private readonly ILogger _logger;

    public void Send(GameSession gameSession)
    {
        _logger.Debug(LoggerLayer.Info, $"Send message from {gameSession.FromInfo.EndPoint} to {gameSession.ToInfo.EndPoint}");
        gameSession.Send(new HelloWorldModel
        {
            InnerStructure = new InnerStructure
            {
                Number = 1
            }
        });
    }
}
```