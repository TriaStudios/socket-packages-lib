﻿using TriaStudios.NetworkLib.Client.Tests.ErrorHandlers.Interfaces;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Shared.Tests.Enums;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.ErrorHandlers;

[Dependency]
public class RegisterLongUsernameErrorHandler : IErrorHandler
{
    public ErrorType ErrorType => ErrorType.RegisterLongUsername;

    public void Handle(Session session, Error error)
    {
        session.Send(new Register
        {
            Name = "FirstLast"
        });
    }
}
