﻿using TriaStudios.NetworkLib.Client.Tests.ErrorHandlers.Interfaces;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Shared.Tests.Enums;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.ErrorHandlers;

[Dependency]
public class RegisterShortUsernameErrorHandler : IErrorHandler
{
    public ErrorType ErrorType => ErrorType.RegisterShortUsername;

    public void Handle(Session session, Error error)
    {
        session.Send(new Register
        {
            Name = "Firstname Lastname"
        });
    }
}
