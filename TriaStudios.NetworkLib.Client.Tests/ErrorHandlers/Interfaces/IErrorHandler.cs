﻿using TriaStudios.NetworkLib.Shared.Tests.Enums;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.ErrorHandlers.Interfaces;

public interface IErrorHandler
{
    ErrorType ErrorType { get; }
    
    void Handle(Session session, Error error);
}
