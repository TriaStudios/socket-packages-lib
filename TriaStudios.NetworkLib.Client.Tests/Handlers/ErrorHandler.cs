﻿using TriaStudios.NetworkLib.Client.Tests.ErrorHandlers.Interfaces;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.Handlers;

public class ErrorHandler : Handler<Error, Session>
{
    private readonly IErrorHandler[] _errorHandlers;
    
    protected override void Handle(Session session, Error model)
    {
        _errorHandlers
            .FirstOrDefault(x => x.ErrorType == model.ErrorCode)?
            .Handle(session, model);
    }
}
