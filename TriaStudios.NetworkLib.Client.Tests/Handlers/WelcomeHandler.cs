﻿using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.Handlers;

/// <summary>
/// Class inheriting from Handler is automatic dependency!
/// It is no need to write [Dependency]
/// </summary>
public class WelcomeHandler : Handler<Welcome, Session>
{
    private readonly ILogger _logger;
    
    protected override void Handle(Session session, Welcome model)
    {
        _logger.Debug(LoggerLayer.Info, "Here you can handle package and send other packages backwards");
        _logger.Debug(LoggerLayer.Info, $"Receive message from {session.ToInfo.EndPoint}");
        session.Send(new Register
        {
            Name = "First"
        });
    }
}
