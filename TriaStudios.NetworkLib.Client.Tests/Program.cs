﻿using System.IO.Compression;
using System.Net;
using TriaStudios.NetworkLib.Client.Tests;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Middlewares;

await NetworkSocket<Session>.Builder()
    .BindAddress(IPEndPoint.Parse("127.0.0.1:1234"))
    .AddLogger((layer, s) => Console.WriteLine($"[{DateTime.Now}] [{layer.ToString().ToUpper()}] {s}"))
    .AddConnectHandler((session, server) =>
    {
        var instance = server.IoCService.GetInstance<ILogger>();
        instance.Debug(LoggerLayer.Debug, $"Connected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
    })
    .AddDisconnectHandler((session, server) =>
    {
        var instance = server.IoCService.GetInstance<ILogger>();
        instance.Debug(LoggerLayer.Debug, $"Disconnected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
    })
    .AddPipeline(builder =>
    {
        // If you use encryption (RSA) it is useless to use compression
        // because encryption text is not to be able to compress more than it is.
        builder.UseEncryption();
        builder.UseCompression(CompressionLevel.Optimal);

        builder.Use((session, data, services) =>
        {
            services.GetInstance<ILogger>().Debug(LoggerLayer.Trace, $"{session.FromInfo.EndPoint} send to {session.ToInfo.EndPoint}");
            return data;
        }, (session, data, services) =>
        {
            services.GetInstance<ILogger>().Debug(LoggerLayer.Trace, $"{session.ToInfo.EndPoint} send to {session.FromInfo.EndPoint}");
            return data;
        });
    })
    .BuildClient()
    .RunAsync();
