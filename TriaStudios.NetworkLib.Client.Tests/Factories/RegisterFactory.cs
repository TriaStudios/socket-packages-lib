﻿using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Client.Tests.Factories;

[Dependency]
public class RegisterFactory
{
    public void Send(Session session)
    {
        session.Send(new Register
        {
            Name = "Firstname Surname"
        });
    }
}
