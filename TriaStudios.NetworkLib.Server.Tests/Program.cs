﻿using System.IO.Compression;
using System.Net;
using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Core.Middlewares;
using TriaStudios.NetworkLib.Server.Tests;
using TriaStudios.NetworkLib.Server.Tests.Factories;
using TriaStudios.NetworkLib.Server.Tests.Middlewares;

await NetworkSocket<Session>.Builder()
    .BindAddress(IPEndPoint.Parse("127.0.0.1:1234"))
    .AddLogger((layer, s) => Console.WriteLine($"[{DateTime.Now}] [{layer.ToString().ToUpper()}] {s}"))
    .AddConnectHandler((session, server) =>
    {
        var instance = server.IoCService.GetInstance<ILogger>();
        instance.Debug(LoggerLayer.Debug, $"Connected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
    })
    .AddDisconnectHandler((session, server) =>
    {
        var instance = server.IoCService.GetInstance<ILogger>();
        instance.Debug(LoggerLayer.Debug, $"Disconnected {session.ToInfo.SourceType} with address {session.ToInfo.EndPoint}");
    })
    .AddPipeline(builder =>
    {
        // If you use encryption (RSA) it is useless to use compression
        // because encryption text is not to be able to compress more than it is.
        builder.UseEncryption();
        builder.UseCompression(CompressionLevel.Optimal);

        // Custom middlewares
        builder.Use(service => new CustomMiddleware1(service));
        builder.Use<CustomMiddleware2>();
    })
    .AddConnectHandler((session, server) => server.IoCService.GetInstance<WelcomeFactory>().Send(session))
    .BuildServer()
    .RunAsync();
