﻿using TriaStudios.NetworkLib.Core.Abstract;

namespace TriaStudios.NetworkLib.Server.Tests;

public class Session : NetworkSession
{
    public string Name { get; set; }
}
