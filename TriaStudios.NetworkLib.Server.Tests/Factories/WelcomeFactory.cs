﻿using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Server.Tests.Factories;

/// <summary>
/// All dependencies are singleton! 
/// </summary>
[Dependency]
public class WelcomeFactory
{
    // You can pass dependencies from IoC like that
    private readonly ILogger _logger;
    
    public void Send(Session session)
    {
        _logger.Debug(LoggerLayer.Info, $"Send message from {session.FromInfo.EndPoint} to {session.ToInfo.EndPoint}");
        session.Send(new Welcome());
    }
}
