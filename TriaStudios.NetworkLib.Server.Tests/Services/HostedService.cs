﻿using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Attributes;
using TriaStudios.NetworkLib.Core.Enums;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Server.Tests.Services;

/// <summary>
/// You can inherit dependency from IoCBehaviour
/// for handling start event
/// </summary>
[Dependency]
public class HostedService : IoCBehaviour
{
    private readonly ILogger _logger;
    
    public override void Start()
    {
        _logger.Debug(LoggerLayer.Info, "Here you can run background processes");
    }
}
