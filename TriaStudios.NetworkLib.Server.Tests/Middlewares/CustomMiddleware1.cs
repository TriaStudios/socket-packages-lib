using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Server.Tests.Middlewares;

public class CustomMiddleware1 : Middleware<Session>
{
    private readonly IIoCService _ioCService;

    public CustomMiddleware1(IIoCService ioCService)
    {
        _ioCService = ioCService;
    }

    public override byte[] Forward(Session session, byte[] data)
    {
        return data;
    }

    public override byte[] Backward(Session session, byte[] data)
    {
        return data;
    }
}
