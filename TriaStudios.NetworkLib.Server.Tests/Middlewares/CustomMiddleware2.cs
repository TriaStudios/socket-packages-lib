using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Core.Interfaces;

namespace TriaStudios.NetworkLib.Server.Tests.Middlewares;

public class CustomMiddleware2 : IMiddleware
{
    public byte[] Forward(NetworkSession session, byte[] data)
    {
        return data;
    }

    public byte[] Backward(NetworkSession session, byte[] data)
    {
        return data;
    }
}
