﻿using TriaStudios.NetworkLib.Core.Abstract;
using TriaStudios.NetworkLib.Shared.Tests.Enums;
using TriaStudios.NetworkLib.Shared.Tests.Models.Authorization;

namespace TriaStudios.NetworkLib.Server.Tests.Handlers;

public class RegisterHandler : Handler<Register, Session>
{
    protected override void Handle(Session session, Register model)
    {
        switch (model.Name?.Length)
        {
            case < 6:
                session.Send(new Error
                {
                    ErrorCode = ErrorType.RegisterShortUsername
                });
                return;
            case > 16:
                session.Send(new Error
                {
                    ErrorCode = ErrorType.RegisterLongUsername
                });
                return;
        }

        if (ConnectedSessions.Any(x => x.Name == model.Name))
        {
            session.Send(new Error
            {
                ErrorCode = ErrorType.RegisterExistUser
            });
            return;
        }

        session.Name = model.Name;
    }
}
